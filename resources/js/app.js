// internal icons
import { faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload, faPencil, faTrashCan, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload, faPencil, faTrashCan, faCircleXmark);

import Axios from 'axios';
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './router';
import moment from 'moment';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import { library } from '@fortawesome/fontawesome-svg-core';

window.axios = Axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('./bootstrap');
} catch (e) {}


Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

Vue.use(VueRouter);
Vue.use(require('vue-moment'));

Vue.prototype.moment = moment;
Vue.component('vue-fontawesome', FontAwesomeIcon);
Vue.component('main-component', require('./components/MainComponent.vue').default);

const app = new Vue({
    el: '#app',
    router: new VueRouter(routes)
});
