import referral from '../views/referral-codes.vue';


export default {
    mode: 'history',
    linkActiveClass: 'accent--text',
    routes: [
        {
            path: '/',
            name: 'referral',
            component: referral
        },
    ]
}