<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReferralCode extends Model
{
    use HasFactory;
    protected $table = "referral_codes";
    protected $fillable = [
        'user_id',
        'referral_code',
        'type',
        'description'
    ];
}
