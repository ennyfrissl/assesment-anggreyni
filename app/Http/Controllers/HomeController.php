<?php

namespace App\Http\Controllers;

use App\Models\ReferralCode;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index()
    {
        $data = ReferralCode::select('users.name', 'referral_codes.*')->join('users', 'referral_codes.user_id', 'users.id')->get();

        return response()->json([
            'data' => $data,
        ]);
    }


    public function store()
    {
        request()->validate([
            'referral_code' => 'required',
            'type'          => 'required',
            'description'   => 'required',
        ]);


        $createType = ReferralCode::create([
            'user_id'       => 1,
            'referral_code' => request('referral_code'),
            'type'          => request('type'),
            'description'   => request('description'),
        ]);

        if ($createType) {
            return response()->json([
                'message'   => 'Referral code created'
            ]);
        } else {
            return response()->json([
                'message'   => 'Something went wrong!'
            ]);
        }
    }

    public function update($id)
    {
        request()->validate([
            'referral_code' => 'required',
            'type'          => 'required',
            'description'   => 'required',
        ]);

        $updateType = ReferralCode::where('id', $id)->update([
            'user_id'       => 1,
            'referral_code' => request('referral_code'),
            'type'          => request('type'),
            'description'   => request('description'),
        ]);

        if ($updateType) {
            return response()->json([
                'message'   => 'Referral code updated'
            ]);
        } else {
            return response()->json([
                'message'   => 'Something went wrong!'
            ]);
        }
    }

    public function destroy($id)
    {
        $deleteType = ReferralCode::where('id', $id)->delete();

        if ($deleteType) {
            return response()->json([
                'message'   => 'Referral code deleted'
            ]);
        } else {
            return response()->json([
                'message'   => 'Something went wrong!'
            ]);
        }
    }
}
