<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::prefix('referral-code')->group(function () {
    Route::get('', 'HomeController@index');
    Route::post('create', 'HomeController@store');
    Route::get('{id}/detail', 'HomeController@detail');
    Route::patch('{id}/update', 'HomeController@update');
    Route::delete('{id}/delete', 'HomeController@destroy');
});
